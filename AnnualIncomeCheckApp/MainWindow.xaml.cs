﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;


namespace AnnualIncomeCheckApp
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        // 入力されたユーザー名
        private String _userName;

        // 入力された年収[万円]
        private int _annualIncome;

        /// <summary>
        /// 画面初期化用メソッド
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // コンテンツに合わせて自動的にWindow幅と高さをリサイズする
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }

        /// <summary>
        /// 年収の演算メソッド
        /// </summary>
        /// <param name="annualIncome"></param>
        /// <returns></returns>
        public DataTable CalculateIncome()
        {
            // 年収[万円]を円単位に変換
            var initAnnualIncome = this._annualIncome * 10000;
            // 1年間の月数
            var months = 12;
            // 1カ月の一般的な業務日数 土日休みを考慮
            var days = 20;
            // 1日の一般的な操業時間
            var hours = 8;

            // 戻り値用のオブジェクト生成
            var resultCalculateSalarys = new DataTable();

            resultCalculateSalarys.Columns.Add(new DataColumn("種別"));
            resultCalculateSalarys.Columns.Add(new DataColumn("金額"));

            // DataTableへの行追加
            resultCalculateSalarys.Rows.Add(new Object[] { "月収", (initAnnualIncome / months).ToString("#,0") + "円"});
            resultCalculateSalarys.Rows.Add(new Object[] { "日給", (initAnnualIncome / months / days).ToString("#,0") + "円" });
            resultCalculateSalarys.Rows.Add(new Object[] { "時給", (initAnnualIncome / months / days / hours).ToString("#,0") + "円" });
            resultCalculateSalarys.Rows.Add(new Object[] { "分給", (initAnnualIncome / months / days / hours / 60).ToString("#,0") + "円" });
            resultCalculateSalarys.Rows.Add(new Object[] { "秒給", (initAnnualIncome / months / days / hours / 60 / 60).ToString("#,0") + "円" });

            return resultCalculateSalarys;
        }

        /// 年収コメント出力メソッド
        /// <summary>
        /// 年収を分析し、アドバイスを返す。
        /// </summary>
        public void outputSalaryComment()
        {
            // 会長・社長クラス　一般平均年収3693万円
            var chairmanClass = 3693;
            // 部長クラス　一般平均年収800万円
            var departmentChiefClass = 800;
            // 従業員クラス　一般平均年収424万円
            var employeeClass = 424;
            // アルバイトクラス 一般平均年収103万円
            var albiteClass = 103;

            // 結果メッセージ出力
            if (this._annualIncome > chairmanClass)
            {
                resultMessageLabel.Text = String.Format("きっと{0}さん、あなたは世界が認める大スターでしょう！脱税だけはしないように！", this._userName);
            }
            else if (this._annualIncome > departmentChiefClass)
            {
                resultMessageLabel.Text = String.Format("{0}さん、あなたはエリート級の人材です！", this._userName);
            }
            else if (this._annualIncome > employeeClass)
            {
                resultMessageLabel.Text = String.Format("{0}さん、あなたは普通な人です！", this._userName);
            }
            else if (this._annualIncome > albiteClass)
            {
                resultMessageLabel.Text = String.Format("{0}さん、あなたはアルバイトレベルの収入ですね・・・笑！", this._userName);
            }
            else
            {
                resultMessageLabel.Text = String.Format("ん〜生きる価値なし。！");
            }
        }

        /// <summary>
        /// 年収入力用のテキストボックスを半角数字以外入力できないように制御するハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AnnualIncommePreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // 正規表現定義(念の為マイナスも入力可能にする)
            Regex regex = new Regex("[^0-9.-]+");
            if (annualIncomeTextBox.Text.Length > 0 && e.Text == "-")
            {
                e.Handled = true;
                return;
            }

            var text = annualIncomeTextBox.Text + e.Text;
            e.Handled = regex.IsMatch(text);
        }

        /// <summary>
        /// 実行ボタンのクリックイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void executeButton_Click(object sender, RoutedEventArgs e)
        {
            // フィールドの取得
            this._userName = this.userNameTextBox.Text.ToString();

            // 年収をint型にパース
            if(int.TryParse(this.annualIncomeTextBox.Text.ToString(), out int outout))
            {
                this._annualIncome = outout;
                this.annualLabel.Background = new SolidColorBrush();

                // 演算処理
                this.resultGrid.ItemsSource = CalculateIncome().DefaultView;

                // 結果メッセージ出力
                outputSalaryComment();

                // コンテンツに合わせて自動的にWindow幅と高さをリサイズする
                this.SizeToContent = SizeToContent.WidthAndHeight;
            }
            else
            {
                // int型にパース出来なかった場合はラベルを色付け
                this.annualLabel.Background = new SolidColorBrush(Colors.Yellow);
            }

        }
    }
}
